# aamohammed4556@gmail.com
# Solves the maze you provide in the maze variable. '1' represents a wall, and '0' represents 
# an empty space

import numpy as np
from itertools import product
import timeit

tic = timeit.default_timer()

START = (1, 1)
END = 2, 5
maze = np.matrix([
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 1, 0, 1, 0, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 0, 0, 0, 1],
    [1, 0, 1, 0, 1, 1, 1, 1, 0, 1],
    [1, 0, 1, 0, 0, 0, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 1, 0, 1, 0, 1],
    [1, 0, 0, 0, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 0, 1, 1, 1, 0, 1],
    [1, 0, 1, 0, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
])

def mark_coord(m, coords, mark):
    m[coords[0], coords[1]] = mark
    return m

def step(m, current_step):
    
    # DONT FORGET Y COMES BEFORE X IN NUMPY

    X, Y= m.shape[1], m.shape[0]

    for x in range(0, X):
        for y in range(0, Y):

            # If the point is equal to the step that the current step, find neighbors and increase them by one 
            # if it is an open spot (represented as 0)

            if m[y, x] == current_step:
                   

                # Finds direct neighbors: I know this is stupid, but I'm too lazy to find another more elegant way
                n = []
                if x - 1 >= 0:
                   n.append((x-1, y))
                if x + 1 <= X:
                   n.append((x+1, y))
                if y - 1 >= 0:
                   n.append((x, y-1))
                if y+1 <= Y:
                    n.append((x, y+1))
                
                for i in n:
                    if m[i[1], i[0]] == 0:
                        m[i[1], i[0]] = current_step-1

                    elif m[i[1], i[0]] == 3:
                        return (m, True)
            else:
                pass


    return (m, False)


maze = mark_coord(maze, START, '-1')
maze = mark_coord(maze, END, '3')

found = False
for i in range(1, 30):
    maze, found = step(maze, -i)
    if found:
        break

else:
    print('Not Found')
print(maze)

tac = timeit.default_timer()
print(tac-tic)



